package com.example.demo2;

import org.junit.jupiter.api.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;

public class MainPageTest {
    private WebDriver driver;
    private MainPage mainPage;



    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://magento.softwaretestingboard.com/");

        mainPage = new MainPage(driver);
    }

//    @AfterEach
//    public void tearDown() {
//        driver.quit();
//    }

    @Test
    public void search() {
        mainPage.inputSearch.click();

        WebElement searchField = driver.findElement(By.cssSelector("input[id='search']"));
        searchField.sendKeys("primo endurance tank");

        WebElement submitButton = driver.findElement(By.cssSelector("button[class$='search']"));
        submitButton.click();

        WebElement selectXL = driver.findElement(By.cssSelector("div[id='option-label-size-143-item-170']"));
        selectXL.click();

        WebElement selectYellow = driver.findElement(By.cssSelector("div[id='option-label-color-93-item-60']"));
        selectYellow.click();

        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(3));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[class*='tocart']"))).click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[data-bind='html: $parent.prepareMessageForHtml(message.text)'] a"))).click();

        wait.until(ExpectedConditions.elementToBeClickable(mainPage.buttonProceedCheckout));
        mainPage.buttonProceedCheckout.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[data-role='opc-continue'] span")));


        wait.until(ExpectedConditions.visibilityOf(mainPage.inputFirstname));
        mainPage.inputFirstname.sendKeys("TestFirstName");

        mainPage.inputLastname.click();
        mainPage.inputLastname.sendKeys("TestLastName");

        mainPage.inputPostcode.click();
        mainPage.inputPostcode.sendKeys("33-333");

        mainPage.inputStreet.click();
        mainPage.inputStreet.sendKeys("test street 11/22");

        mainPage.inputTelephone.click();
        mainPage.inputTelephone.sendKeys("4888788898");

        mainPage.inputCity.click();
        mainPage.inputCity.sendKeys("Krakow");

        Select sCountry = new Select(mainPage.selectCountry);
        sCountry.selectByVisibleText("Poland");

        Select sState = new Select(mainPage.selectRegion);
        sState.selectByVisibleText("małopolskie");

        mainPage.inputCustomerEmail.sendKeys("test@test.test");

        wait.until(ExpectedConditions.elementToBeClickable(mainPage.spanNext)).click();

        WebElement radioElement = driver.findElement(By.name("ko_unique_1"));
        boolean selectState = radioElement.isSelected();

        if(selectState == false) {
            radioElement.click();
        }

        wait.until(ExpectedConditions.elementToBeClickable(mainPage.spanNext)).click();


        wait.until(ExpectedConditions.invisibilityOf(mainPage.Loader));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/main/div[2]/div/div[2]")));
        mainPage.divPlaceOrder.click();

        wait.until(ExpectedConditions.elementToBeClickable(mainPage.buttonPlaceOrder)).click();



    }
}
