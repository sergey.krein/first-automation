package com.example.demo2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

// page_url = https://magento.softwaretestingboard.com/
public class MainPage {

    @FindBy(css = "input[id='search']")
    public WebElement inputSearch;


    @FindBy(css = "button[data-role='proceed-to-checkout']")
    public WebElement buttonProceedCheckout;

    @FindBy(css = "input[data-bind$='emailFocused']")
    public WebElement inputCustomerEmail;

    @FindBy(name = "firstname")
    public WebElement inputFirstname;

    @FindBy(name = "lastname")
    public WebElement inputLastname;

    @FindBy(name = "street[0]")
    public WebElement inputStreet;

    @FindBy(name = "city")
    public WebElement inputCity;

    @FindBy(name = "country_id")
    public WebElement selectCountry;

    @FindBy(name = "region_id")
    public WebElement selectRegion;

    @FindBy(name = "postcode")
    public WebElement inputPostcode;

    @FindBy(name = "telephone")
    public WebElement inputTelephone;

    @FindBy(css = "button[data-role='opc-continue'] span")
    public WebElement spanNext;

    @FindBy(xpath = "/html/body/div[1]")
    public WebElement Loader;

    @FindBy(xpath = "/html/body/div[3]/main/div[2]/div/div[2]/div[4]/ol/li[3]/div/form/fieldset/div[1]/div/div/div[2]/div[2]/div[4]/div/button")
    public WebElement buttonPlaceOrder;

    @FindBy(xpath = "/html/body/div[3]/main/div[2]/div/div[2]")
    public WebElement divPlaceOrder;
    
    
    
    

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
